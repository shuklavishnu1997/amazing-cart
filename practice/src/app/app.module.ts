import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemplateformComponent } from './Components/templateform/templateform.component';
import { TempvalidationsComponent } from './Components/tempvalidations/tempvalidations.component';
import { ModelformComponent } from './Components/modelform/modelform.component';
import { NestedreactiveComponent } from './Components/nestedreactive/nestedreactive.component';
import { ReactiveValidationComponent } from './Components/reactive-validation/reactive-validation.component';
import { ElectronicsComponent } from './Components/electronics/electronics.component';
import { FashionComponent } from './Components/fashion/fashion.component';
import { FootwearComponent } from './Components/footwear/footwear.component';
import { HomeComponent } from './Components/home/home.component';
import { RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { SearchComponent } from './Components/search/search.component';
import { CategoriesComponent } from './Components/categories/categories.component';
import { ProductsComponent } from './Components/products/products.component';
import { ProductsetailsComponent } from './Components/productsdetails/productsdetails.component';
import { MangerComponent } from './Components/manger/manger.component';
import { LoginComponent } from './Components/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    TemplateformComponent,
    TempvalidationsComponent,
    ModelformComponent,
    NestedreactiveComponent,
    ReactiveValidationComponent,
    ElectronicsComponent,
    FashionComponent,
    FootwearComponent,
    HomeComponent,
    NotFoundComponent,
    SearchComponent,
    CategoriesComponent,
    ProductsComponent,
    ProductsetailsComponent,
    MangerComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
