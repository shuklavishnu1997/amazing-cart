 import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManagerGuard implements CanActivate {
  constructor( private router:Router){}
  users=[
    {Name:'john',Role:'admin',Password:'admin12'},
    {Name:'david',Role:'manager',Password:'mng12'}
  ];
  userName='david';
  password='mng12';
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):boolean {
    for(var user of this.users){
      if(user.Name==this.userName&&user.Password==this.password){
        return true;
      }
    }
    this.router.navigate(['login']);
  }

  
}
