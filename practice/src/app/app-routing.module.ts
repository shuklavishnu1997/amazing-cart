import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManagerComponent } from 'projects/shopping/src/app/manager/manager.component';
import { CategoriesComponent } from './Components/categories/categories.component';
import { ElectronicsComponent } from './Components/electronics/electronics.component';
import { FashionComponent } from './Components/fashion/fashion.component';
import { FootwearComponent } from './Components/footwear/footwear.component';
import { HomeComponent } from './Components/home/home.component';
import { LoginComponent } from './Components/login/login.component';
import { ProductsComponent } from './Components/products/products.component';
import { ProductsetailsComponent } from './Components/productsdetails/productsdetails.component';
import { SearchComponent } from './Components/search/search.component';
import { ManagerGuard } from './manager.guard';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {path:'home',component: HomeComponent},
  {path:'electronics', component: ElectronicsComponent},
  {path:'footwear',component: FootwearComponent},
  {path:'fashion',component:FashionComponent},
  {path:'categories',component:CategoriesComponent},
  {path:'login',component:LoginComponent},
  {path:'manager',component:ManagerComponent,canActivate:[ManagerGuard]},
  {path:'categories/:id',component:ProductsComponent,
   children:[
     {path:'details/:id',component:ProductsetailsComponent}
   ]   
  },
  {path:'search/:category/:model/:price',component:SearchComponent},
  {path:'',redirectTo:'home',pathMatch:'full'},
  {path:'**',component:NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
