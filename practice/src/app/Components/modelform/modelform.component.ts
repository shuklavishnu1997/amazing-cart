import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms'
@Component({
  selector: 'app-modelform',
  templateUrl: './modelform.component.html',
  styleUrls: ['./modelform.component.css']
})
export class ModelformComponent  {
  
  frmRegister=new FormGroup({
    txtName: new FormControl(''),
    lstCities:new FormControl(''),
    InStock :new FormControl('')
  })

  onSubmit(obj){
   alert(JSON.stringify(obj)); 
  }
}
