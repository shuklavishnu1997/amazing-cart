import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
 categories=[
   { CategoryId:1, CategoryName:'Electronics'},
   { CategoryId:2, CategoryName:'Fashion'},
   { CategoryId:3, CategoryName:'Footwear'}
 ]
}
