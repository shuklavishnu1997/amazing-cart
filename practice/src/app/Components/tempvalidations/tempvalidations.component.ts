import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tempvalidations',
  templateUrl: './tempvalidations.component.html',
  styleUrls: ['./tempvalidations.component.css']
})
export class TempvalidationsComponent {
cityError=true;
evenError=true;
onCityChange(value){
  if(value=="notcity"){
    this.cityError=true;
  }else{
    this.cityError=false;
  }
}
onEvenChange(val){
  if(val%2==0){
     this.evenError=false;
  }else{
    this.evenError=true;
  }
}
}
