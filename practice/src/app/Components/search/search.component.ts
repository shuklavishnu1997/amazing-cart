import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private route:ActivatedRoute) { }
   category;
   model;
   price;
  ngOnInit(): void {
    this.category=this.route.snapshot.paramMap.get('category');
    this.model=this.route.snapshot.paramMap.get('model');
    this.price=this.route.snapshot.paramMap.get('price')
  }

}
