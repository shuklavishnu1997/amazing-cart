import { Component, OnInit } from '@angular/core';
import { CaptchaService } from '../../services/captcha.service';
import { DataService } from '../../services/data.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private captcha:CaptchaService,private data:DataService) { }
   code;
   products=[];
  ngOnInit(): void { 
    this.code= this.captcha.GenerateCode();
    this.products=this.data.GetData();
  }
  NewCode(){
    this.code=this.captcha.GenerateCode();
  }

}
