import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControlName, FormControl } from '@angular/forms'
@Component({
  selector: 'app-nestedreactive',
  templateUrl: './nestedreactive.component.html',
  styleUrls: ['./nestedreactive.component.css']
})
export class NestedreactiveComponent {

frmRegister= new FormGroup({
  Name:new FormControl(''),
  Price:new FormControl(''),
  frmDetails:new FormGroup({
   City:new FormControl('') ,
    Stock:new FormControl('')
  })
})
}
