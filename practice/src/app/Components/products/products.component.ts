import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  data=[
    {ProductId:1,Name:'JBL Speaker',Price:5000,Photo:"assets/images/jbl.jpg",CategoryId:1},
    {ProductId:2,Name:'Mobile',Price:40000,Photo:"assets/images/mobile.jpg",CategoryId:1},
     {ProductId:3,Name:'Shirt',Price:3000,Photo:"assets/images/shirt.jpg",CategoryId:2},
     {ProductId:4,Name:'Jeans',Price:6000,Photo:"assets/images/jeans.jpg",CategoryId:2},
     {ProductId:5,Name:'Nike',Price:7000,Photo:"assets/images/nike4.jpg",CategoryId:3},
     {ProductId:6,Name:'Killer',Price:4000,Photo:"assets/images/killer.jpg",CategoryId:3}
  ];
  constructor(private route:ActivatedRoute,private router:Router) { }
  categoryId;
   products=[];
  ngOnInit(): void {
    this.categoryId=this.route.snapshot.paramMap.get('id');
    this.products=this.data.filter(x=>x.CategoryId==this.categoryId);
  }
GetDetails(item){
  this.router.navigate(['details',item.ProductId],{relativeTo:this.route})
}
}
